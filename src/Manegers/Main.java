package Manegers;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.ArrayList;

public class Main extends Application {

    private static ArrayList<SceneControl> sceneControl = new ArrayList<>();
    private static Stage stageHandler;

    @Override
    public void start(Stage stage) throws Exception {
        stageHandler = stage;

        FXMLLoader mainDishLoader = new FXMLLoader(getClass().getResource("../FXMLFiles/MainDish.fxml"));
        Parent mainDishPane = mainDishLoader.load();
        Scene mainDishScene = new Scene(mainDishPane,700,700);

        FXMLLoader sideDishLoader = new FXMLLoader(getClass().getResource("../FXMLFiles/SideDish.fxml"));
        Parent sideDishPane = sideDishLoader.load();
        Scene sideDishScene = new Scene(sideDishPane,700,700);

        FXMLLoader extraDishLoader = new FXMLLoader(getClass().getResource("../FXMLFiles/ExtraDish.fxml"));
        Parent extraDishPane = extraDishLoader.load();
        Scene extraDishScene = new Scene(extraDishPane,700,700);

        FXMLLoader dessertsDishLoader = new FXMLLoader(getClass().getResource("../FXMLFiles/Desserts.fxml"));
        Parent dessertsDishPane = dessertsDishLoader.load();
        Scene dessertsDishScene = new Scene(dessertsDishPane,700,700);


        sceneControl.add(new SceneControl(mainDishScene,ControllerName.MainDish));
        sceneControl.add(new SceneControl(sideDishScene,ControllerName.SideDish));
        sceneControl.add(new SceneControl(extraDishScene,ControllerName.ExtraDish));
        sceneControl.add(new SceneControl(dessertsDishScene,ControllerName.Desserts));

        stage.setTitle("MainDIsh");
        stage.setResizable(false);
        stage.setScene(mainDishScene);
        stage.show();
    }

    public static void changeScene(ControllerName sceneName) {
        stageHandler.setTitle(sceneName.toString());

        for (SceneControl sceneCtrlNamePair : sceneControl) {
            if (sceneCtrlNamePair.getName().equals(sceneName)) {
                stageHandler.setScene(sceneCtrlNamePair.getScene());
            }
        }
    }

}
