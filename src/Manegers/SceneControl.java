package Manegers;

import javafx.scene.Scene;

public class SceneControl {

    private Scene scene;
    private ControllerName name;

    public Scene getScene() {
        return scene;
    }

    public ControllerName getName() {
        return name;
    }

    public SceneControl(Scene scene, ControllerName name) {
        this.scene = scene;
        this.name = name;
    }
}
