package FXMLControllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import Manegers.*;
import javafx.scene.shape.Circle;

import java.awt.event.MouseEvent;

public class MainDishController{

    private int btnCnt;

    @FXML
    public Button sideDish,desserts,extraDish, testButton, test;


    public void changeToSideDish(ActionEvent event){
        Main.changeScene(ControllerName.SideDish);
    }

    public void changeToExtraDish(ActionEvent event){
        Main.changeScene(ControllerName.ExtraDish);
    }

    public void changeToDesserts(){
        Main.changeScene(ControllerName.Desserts);

    }

    public void changeOnClick(){

        btnCnt++;
        if( btnCnt%2 == 1 ) {
            testButton.setStyle("-fx-background-color: blue");
        }
        else{
            testButton.setStyle("-fx-background-color: white");
        }
    }
}
