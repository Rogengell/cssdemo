package FXMLControllers;

import Manegers.ControllerName;
import Manegers.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

import java.awt.*;

public class DessertsController {

    @FXML
    public Button mainDish,sideDish,extraDish;

    public void changeToSideDish(ActionEvent event){
        Main.changeScene(ControllerName.SideDish);
    }

    public void goBackToMain(ActionEvent event){
        Main.changeScene(ControllerName.MainDish);
    }

    public void changeToExtraDish(ActionEvent event){
        Main.changeScene(ControllerName.ExtraDish);
    }
}
