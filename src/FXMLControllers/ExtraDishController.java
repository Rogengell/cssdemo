package FXMLControllers;

import Manegers.ControllerName;
import Manegers.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class ExtraDishController {

    @FXML
    public Button mainDish,sideDish,desserts;

    public void changeToDesserts(ActionEvent event){
        Main.changeScene(ControllerName.Desserts);
    }

    public void changeToSideDish(ActionEvent event){
        Main.changeScene(ControllerName.SideDish);
    }

    public void goBackToMain(ActionEvent event){
        Main.changeScene(ControllerName.MainDish);
    }
}

