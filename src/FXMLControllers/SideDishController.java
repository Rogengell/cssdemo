package FXMLControllers;

import Manegers.ControllerName;
import Manegers.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class SideDishController {

    @FXML
    public Button mainDish,extraDish,desserts;


    public void changeToExtraDish(ActionEvent event){
        Main.changeScene(ControllerName.ExtraDish);
    }

    public void changeToDesserts(ActionEvent event){
        Main.changeScene(ControllerName.Desserts);

    }

    public void goBackToMain(ActionEvent event){
        Main.changeScene(ControllerName.MainDish);
    }
}
